# TypeScript

Le point d'interrogation veut dire que c'est optionnelle / facultatif

    function createUser(name: string, age: number, address?: string);
        
        return {
            name,
            age,
            address
        };
    }
    
    createUser(name: 'Jean', age: 12);

### ts config

-h permet d'avoir la liste des commandes

-w permet d'écouter des fichiers pour savoir ou se trouve les erreurs

-pretty permet de stylisé les mss d'erreur avec de la couleur

-v permet d'afficher la version du compilateur

--init permet d'initialiser le fichier ts config

-b permet de build notre propre jeu

-t permet de signifier la cible