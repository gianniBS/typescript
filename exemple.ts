//basic

let nb: number = 3;
nb = 4;


let arr: string[][];
arr = [];
arr.push(["ok"]);


let obj: {
    name: string,
    age?: number | string
};

obj = {name: "Jean", age: 12};

//function

function createUser(name: string, age: number, address?: string):
    {name: string, age: number, address: string} {

    return {
        name,
        age,
        address
    };
}
let fn : (nb: string) => boolean | number;

//enums

enum Direction {
    TOP = 100,
    BOTTOM,
    RIGHT,
    LEFT,
}
let userDirection: Direction = Direction.LEFT;

//tuples

let arr1: [number[], string, number[]] = [[12], "ok", [12]];

//literal types

let transition: 'ease_in' | 'ease-out';

function transformX(transition: 'ease_in' | 'ease-out'): void {
}

//types assertions

let input: string = "24";
let var1: number;

var1 = (input as any);

//interfaces

interface IVehicule {
    model: string;

    move(): string;
}

let car: IVehicule;
car = {
    model: 'BMW',
    move: function () {
        return this.model;
    }
};

function garage(vehicule: IVehicule): IVehicule {

    vehicule.move();
    vehicule.model;
    return vehicule;
}

//class

class Cord {
    x: string;
    y: string;
}

let cord: Cord;

cord = {
    x: "ok",
    y: "ok",
};

//mais c'est mieux de le marquer comme ça

interface Cord1 {
    x: string;
    y: string;
}

let cord1: Cord1;

cord1 = {
    x: "ok",
    y: "ok",
};

//parameter properties

class Person {

    constructor(public  name: string, protected age: number) {
    }

}

//import export modules

//export modules

export interface ITe {
    model: string;
    auto: number;
}

class te implements ITe {

    constructor(public model: string, public auto: number) {}
}

//import modules

import ITel from './b';

let tel = new ITel('nokia', 6);