class Cord {
    x: string;
    y: string;
}

let cord: Cord;

cord = {
    x: "ok",
    y: "ok",
};

export interface ITel {
    model: string;
    auto: number;
}

export class Tel implements ITel {

    constructor(public model: string, public auto: number) {}
}